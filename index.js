/**
 * @format
 */

import 'react-native-gesture-handler';
import { AppRegistry } from 'react-native';
import Application from './src/Containers/Application';
import { name as appName } from './app.json';

console.disableYellowBox = true;
console.ignoredYellowBox = ['Setting a timer'];
console.ignoredYellowBox = ['Remote debugger'];

AppRegistry.registerComponent(appName, () => Application);
