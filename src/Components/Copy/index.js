import React from 'react';
import {
	TouchableOpacity,
	Text
} from 'react-native';
import styles from './styles';

const CopyFooter = () => {
	return (
	    <TouchableOpacity style={styles.view}>
          <Text style={styles.footerViewText}>Хамелеон 2020 ©</Text>
        </TouchableOpacity>
	)
};

export default CopyFooter;