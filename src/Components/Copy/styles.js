import { StyleSheet, Dimensions } from "react-native";
import { theme } from "../../Services";

let { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  footerViewText: {
    color: "#404040",
    fontFamily: theme.fonts.GilroyBold,
    marginBottom: 7,
    fontSize: 14,
  },
  view: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default styles;
