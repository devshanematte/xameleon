import React, { useMemo } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import styles from "./styles";

const DefaultHeader = ({ navigation }) => {
  const dispatch = useDispatch();
  let user = useSelector((state) => {
    return state.user;
  });

  let getRoutes = useSelector((state) => {
    return state.routes;
  });

  const goRoute = (route) => {
    dispatch({
      type: "UPDATE_CURRENT_NAVIGATION",
      route: route,
    });
    return navigation.navigate(route);
  };

  return useMemo(() => (
    <View style={styles.headerView}>
      <View style={styles.headerViewLeft}>
        <TouchableOpacity
          onPress={() => {
            goRoute("home");
          }}
          style={[
            styles.button,
            styles.buttonLeft,
            getRoutes.currentNavigation == "home"
              ? styles.currentNavigationSelected
              : {},
          ]}
        >
          <View style={styles.viewImageContent}>
            <Image
              style={styles.image}
              source={require("../../assets/avatars/1.png")}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            goRoute("products");
          }}
          style={[
            styles.button,
            styles.buttonLeft,
            getRoutes.currentNavigation == "products"
              ? styles.currentNavigationSelected
              : {},
          ]}
        >
          <View style={styles.viewImageContent}>
            <Image
              style={styles.image}
              source={require("../../assets/all.png")}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.headerViewRight}>
        {/*
						<TouchableOpacity style={[styles.button, styles.buttonRight, styles.buttonOrange]}>
							<Text style={styles.buttonText}>2</Text>
						</TouchableOpacity>
					*/}

        <TouchableOpacity
          onPress={() => {
            goRoute("ration");
          }}
          style={[
            styles.button,
            styles.buttonRightGreen,
            styles.buttonBorderGreen,
            getRoutes.currentNavigation == "ration"
              ? styles.currentNavigationSelected
              : {},
          ]}
        >
          <Text style={[styles.buttonText, styles.buttonTextWhite]}>15</Text>
          {/*
					<View style={styles.buttonStatusOrangeEvent}></View>
					<View style={styles.buttonStatusBlueEvent}></View>
						*/}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            goRoute("profile");
          }}
          style={[
            styles.button,
            styles.buttonRight,
            getRoutes.currentNavigation == "profile"
              ? styles.currentNavigationSelected
              : {},
          ]}
        >
          <View style={styles.viewImageContent}>
            <Image
              style={styles.image}
              source={require("../../assets/userIcon.png")}
            />
            {/* TODO: return everything back to normal */}
            {/* <Image style={styles.image} source={{ 
							uri:user.user.avatar
						}}/> */}
          </View>
        </TouchableOpacity>
      </View>
    </View>
  ));
};

export default DefaultHeader;
