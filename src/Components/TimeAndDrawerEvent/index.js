import React, {
	useMemo
} from 'react';
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	TextInput
} from 'react-native';
import { useSelector } from 'react-redux';
import styles from './styles';
import moment from 'moment';

const TimeAndDrawerEvent = ({
	navigation,
	allAndSearch = false
}) => {

	let user = useSelector((state)=>{
		return state.user.user
	});

	return useMemo(()=>(
		<View style={styles.view}>

			<View style={styles.viewLeft}>
				<TouchableOpacity onPress={()=>{navigation.toggleDrawer()}} style={styles.toogleDrawerStyleButton}>
					<Image style={styles.toogleDrawerStyleButtonImage} source={require('../../assets/menu.png')} />
				</TouchableOpacity>	
				{
					allAndSearch ?
						<Text style={styles.textAllOption}>Общий каталог</Text>
					:
						<View></View>
				}
			</View>

			<View style={styles.viewRight}>
				{
					allAndSearch ?
						<View style={styles.viewRightInputForm}>
							<TextInput style={styles.viewRightInputStyle} placeholder="Поиск по каталогу"/>
							<TouchableOpacity style={styles.viewRightInputFormButton}>
								<Image style={styles.viewRightInputFormButtonImage} source={require('../../assets/search.png')} />
							</TouchableOpacity>
						</View>
					:
						<View style={styles.timeview}>
							<Text style={styles.timeTextTime}>{ moment().format('HH:mm') }</Text>
							<Text style={styles.timeTextDate}>{ moment().format('DD:MM:YYYY') }</Text>
						</View>
				}

			</View>

		</View>
	));

}

export default TimeAndDrawerEvent;