import { 
	StyleSheet, 
	Dimensions 
} from 'react-native';
import { theme } from '../../Services';

let { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
	view:{
		width:'100%',
		position:'relative',
		padding:20,
		flexDirection:'row',
		justifyContent:'space-between'
	},
	toogleDrawerStyleButton:{
		width:40,
		height:50,
		backgroundColor:'#fff',
		borderTopRightRadius:20,
		borderBottomRightRadius:20,
		marginLeft:-15,
		justifyContent:'center',
		alignItems:'center'
	},
	timeview:{
		justifyContent:'flex-end',
		alignItems:'flex-end',
	},
	timeTextTime:{
		fontSize:18,
		color:'#24a8ed',
		fontFamily:theme.fonts.GilroyMedium
	},
	timeTextDate:{
		fontSize:16,
		color:'rgba(0,0,0,0.4)',
		fontFamily:theme.fonts.GilroyMedium	
	},
	toogleDrawerStyleButtonImage:{
		width:17,
		height:25,
		resizeMode:'contain'
	},
	viewLeft:{
		flexDirection:'row',
		alignItems:'center'
	},
	textAllOption:{
		marginLeft:10,
		fontSize:22,
		width:105,
		color:'#808080',
		fontFamily:theme.fonts.GilroyBold,
		lineHeight:25
	},
	viewRight:{
		flex:1
	},
	viewRightInputForm:{
		width:'100%',
		height:50,
		position:'relative'
	},
	viewRightInputStyle:{
		width:'100%',
		height:'100%',
		backgroundColor:'#fff',
		borderRadius:30,
		paddingHorizontal:15
	},
	viewRightInputFormButton:{
		position:'absolute',
		top:0,
		right:0,
		width:50,
		height:50,
		zIndex:100
	},
	viewRightInputFormButtonImage:{
		width:'100%',
		height:'100%',
		resizeMode:'contain'
	}
});

export default styles;
