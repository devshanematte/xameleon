import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { StatusBar } from 'react-native';
import { createStore } from '../Services';
import Context from './Context';

const Application = () => {

  const { store, persistor } = createStore();

  return (
    <Provider store={store}>
      	<PersistGate persistor={persistor}>
      		<StatusBar backgroundColor="#fff" barStyle="dark-content"/>
          	<Context />
      	</PersistGate>
    </Provider>
  );

}

export default Application;
