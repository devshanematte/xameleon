import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Text, View, TextInput, TouchableOpacity } from "react-native";

import * as yup from "yup";
import { Formik } from "formik";

import styles from "../../styles";
import {
  GoogleSignin,
  statusCodes,
} from "@react-native-community/google-signin";
import { api } from "../../../../../Services";

const reviewSchema = yup.object({
  email: yup.string().required().email(),
  password: yup.string().required().max(15).min(8),
});

const LoginForm = ({ navigation }) => {
  const dispatch = useDispatch();

  const signIn = async () => {
    GoogleSignin.configure({
      webClientId:
        "285000749356-ejdfg37vmbku0sui4oqf2aagbnk6ho6k.apps.googleusercontent.com",
      offlineAccess: true,
    });

    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      return dispatch(api.auth.login(userInfo.user, "GOGGLE"));
    } catch (error) {
      console.log(error);
      alert("Сервис пока недоступен");
      return;
    }
  };

  return (
    <View style={styles.formView}>
      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={reviewSchema}
        onSubmit={(values, action) => {
          console.log(values.email);
          console.log(values.password);
          //action.resetForm();
          return dispatch(api.auth.login({
            email:values.email, 
            password:values.password
          }, 'authentication'));
        }}
      >
        {(props) => (
          <View>
            <View style={styles.initialView}>
              <Text style={styles.labelView}>Email</Text>
              <TextInput
                style={styles.inputControl}
                value={props.values.email}
                onChangeText={props.handleChange("email")}
                placeholder="email"
                placeholderTextColor="#333"
              />
            </View>
            <View style={styles.initialView}>
              <Text style={styles.labelView}>Пароль (Не помню пароль)</Text>
              <TextInput
                style={styles.inputControl}
                value={props.values.password}
                secureTextEntry={true}
                onChangeText={props.handleChange("password")}
                placeholder="Пароль"
                placeholderTextColor="#333"
              />
            </View>
            <TouchableOpacity
              onPress={()=>{props.handleSubmit()}}
              style={styles.buttonSubmit}
            >
              <Text style={styles.buttonSubmitText}>Войти</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
      {/* <View style={styles.formViewSection}>
        <Text style={styles.inputStyleLabel}>Телефон</Text>
        <TextInput
          style={styles.inputStyle}
          keyboardType="numeric"
          placeholder="+79454563343"
        />
      </View>

      <View style={styles.formViewSection}>
        <View style={styles.formViewSectionLabelCustom}>
          <Text style={styles.inputStyleLabel}>Пароль</Text>
          <TouchableOpacity>
            <Text style={styles.formViewSectionLabelCustomText}>
              {" "}
              (Не помню пароль)
            </Text>
          </TouchableOpacity>
        </View>
        <TextInput style={styles.inputStyle} secureTextEntry={true} />
      </View>

      <TouchableOpacity style={styles.buttonLogin}>
        <Text style={styles.buttonLoginTitle}>Войти</Text>
      </TouchableOpacity> */}
      <View style={styles.viewOr}>
        <Text style={styles.viewOrText}>или</Text>
      </View>

      <View style={styles.socialsView}>
        <TouchableOpacity
          onPress={() => {
            signIn();
          }}
          style={[styles.ButtonSocialView, styles.buttonGoogle]}
        >
          <Text style={[styles.ButtonSocialViewText]}>G</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.ButtonSocialView, styles.buttonTwitter]}
        >
          <Text style={[styles.ButtonSocialViewText]}>TW</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.ButtonSocialView, styles.buttonFB]}>
          <Text style={[styles.ButtonSocialViewText]}>FB</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginForm;
