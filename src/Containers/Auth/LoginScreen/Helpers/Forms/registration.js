import React from "react";
import { Text, View, TextInput, TouchableOpacity } from "react-native";
import { useDispatch } from 'react-redux';

import * as yup from "yup";
import { Formik } from "formik";

import styles from "../../styles";
import { api } from "../../../../../Services";

const reviewSchema = yup.object({
  firstname: yup.string().required().min(2).max(12),
  email: yup.string().required().email(),
  lastname: yup.string().required().min(2).max(12),
  password: yup.string().required().min(8).max(15),
});

const RegistrationForm = () => {

  let dispatch = useDispatch();

  return (
    <View style={styles.formView}>
      <Formik
        initialValues={{ lastname: "", email: "", firstname: "", password: "" }}
        validationSchema={reviewSchema}
        onSubmit={(values, action) => {
          console.log(values.email);
          console.log(values.name);
          console.log(values.password);
          return dispatch(
            api.auth.login({
              email: values.email,
              lastname: values.lastname,
              firstname: values.firstname,
              password: values.password
            }),
            'registration'
          );
          //action.resetForm();
        }}
      >
        {(props) => (
          <View>
            <View style={styles.initialView}>
              <Text style={styles.labelView}>Имя</Text>
              <TextInput
                style={styles.inputControl}
                value={props.values.firstname}
                onChangeText={props.handleChange("firstname")}
                placeholder="Имя"
                placeholderTextColor="#333"
              />
            </View>
            <View style={styles.initialView}>
              <Text style={styles.labelView}>Фамилия</Text>
              <TextInput
                style={styles.inputControl}
                value={props.values.lastname}
                onChangeText={props.handleChange("lastname")}
                placeholder="Фамилия"
                placeholderTextColor="#333"
              />
            </View>
            <View style={styles.initialView}>
              <Text style={styles.labelView}>Email</Text>
              <TextInput
                style={styles.inputControl}
                value={props.values.email}
                onChangeText={props.handleChange("email")}
                placeholder="Email"
                placeholderTextColor="#333"
              />
            </View>
            <View style={styles.initialView}>
              <Text style={styles.labelView}>Пароль</Text>
              <TextInput
                secureTextEntry={true}
                style={styles.inputControl}
                value={props.values.password}
                onChangeText={props.handleChange("password")}
                placeholder="Пароль"
                placeholderTextColor="#333"
              />
            </View>
            <TouchableOpacity
              onPress={props.handleSubmit}
              style={styles.buttonSubmit}
            >
              <Text style={styles.buttonSubmitText}>Зарегистрироваться</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
      {/* <View style={styles.formViewSection}>
				<Text style={styles.inputStyleLabel}>Телефон</Text>
				<TextInput style={styles.inputStyle} keyboardType="numeric" placeholder="+79454563343"/>
			</View>

			<View style={styles.formViewSection}>
				<Text style={styles.inputStyleLabel}>Пароль</Text>
				<TextInput style={styles.inputStyle} secureTextEntry={true} />
			</View>

			<View style={styles.formViewSection}>
				<Text style={styles.inputStyleLabel}>Повторите пароль</Text>
				<TextInput style={styles.inputStyle} secureTextEntry={true} />
			</View>

			<TouchableOpacity style={styles.buttonLogin}>
				<Text style={styles.buttonLoginTitle}>Регистрация</Text>
			</TouchableOpacity> */}
    </View>
  );
};

export default RegistrationForm;
