import React, { useMemo, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { useDispatch } from "react-redux";
import styles from "./styles";
import { api } from "../../../Services";
import LoginForm from "./Helpers/Forms/login";
import RegistrationForm from "./Helpers/Forms/registration";
import { CopyFooter } from "../../../Components";
import Logo from "../../../assets/logo.svg";
const LoginScreen = ({ navigation }) => {
  let dispatch = useDispatch();
  let [tabSelect, setTabSelect] = useState("login");

  return useMemo(
    () => (
      <View style={styles.view}>
        <View style={styles.headerLogin}>
          <Logo />
        </View>

        <View style={styles.authContentView}>
          <View>
            <View style={styles.tabs}>
              <View style={styles.tabButtons}>
                <TouchableOpacity
                  onPress={() => {
                    setTabSelect("login");
                  }}
                  style={[
                    styles.tabButton,
                    tabSelect == "login" ? styles.selectTabButton : {},
                  ]}
                >
                  <Text
                    style={[
                      tabSelect == "login"
                        ? styles.selectTabButtonText
                        : styles.tabButtonText,
                    ]}
                  >
                    Вход
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    setTabSelect("registration");
                  }}
                  style={[
                    styles.tabButton,
                    tabSelect != "login" ? styles.selectTabButton : {},
                  ]}
                >
                  <Text
                    style={[
                      tabSelect == "registration"
                        ? styles.selectTabButtonText
                        : styles.tabButtonText,
                    ]}
                  >
                    Регистрация
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.tabContent}>
              {tabSelect == "login" ? (
                <LoginForm navigation={navigation} />
              ) : (
                <RegistrationForm navigation={navigation} />
              )}
            </View>
          </View>

          <View style={styles.footerView}>
            <CopyFooter />

            {/* <TouchableOpacity>
              <Text style={styles.footerViewText}>
                Политика конфиденциальности
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.footerViewText}>
                Информация о юридическом лице
              </Text>
            </TouchableOpacity> */}
          </View>
        </View>
      </View>
    ),
    [tabSelect]
  );
};

export default LoginScreen;
