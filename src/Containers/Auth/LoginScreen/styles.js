import { StyleSheet, Dimensions } from "react-native";
import { theme } from "../../../Services";

let { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: "#fff",
    position: "relative",
  },
  headerLogin: {
    width: "100%",
    paddingTop: 15,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    backgroundColor: "#fff",
    //shadowColor: "#000",
    //shadowOffset: {
    //	width: 0,
    //	height: 5,
    //},
    //shadowOpacity: 0.34,
    //shadowRadius: 6.27,
    //elevation: 10,
  },
  headerLoginLogo: {
    width: 291,
    height: 100,
    resizeMode: "contain",
  },
  tabs: {
    width: "100%",
    paddingHorizontal: 20,
    marginTop: 53,
    alignItems: "center",
  },
  tabButtons: {
    width: "80%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  tabButton: {
    width: "45%",
    height: 43,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
  },
  selectTabButton: {
    backgroundColor: "#24A8ED",
  },
  selectTabButtonText: {
    color: "#fff",
    fontFamily: theme.fonts.GilroyRegular,
    fontSize: 14,
    lineHeight: 14,
  },
  tabButtonText: {
    color: "#000000",
    fontFamily: theme.fonts.GilroyRegular,
    fontSize: 14,
    lineHeight: 14,
  },
  tabContent: {
    width: "100%",
    paddingHorizontal: 20,
    marginTop: 10,
    alignItems: "center",
  },
  footerView: {
    //position:'absolute',
    //bottom:0,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
  },
  footerViewText: {
    color: "#404040",
    fontFamily: theme.fonts.GilroyBold,
    marginBottom: 7,
    fontSize: 24,
  },
  formView: {
    width: "80%",
  },
  formViewSection: {
    width: "100%",
    marginBottom: 5,
    marginTop: 5,
  },
  authContentView: {
    flex: 1,
    width: "100%",
    justifyContent: "space-between",
  },
  inputStyle: {
    width: "100%",
    height: 70,
    fontSize: 24,
    backgroundColor: "rgba(0,0,0,0.04)",
    color: "#000000",
    borderRadius: 35,
    paddingHorizontal: 15,
    fontFamily: theme.fonts.GilroyLight,
  },
  inputStyleLabel: {
    marginBottom: 2,
    fontSize: 21,
    color: "rgba(0,0,0,0.64)",
    fontFamily: theme.fonts.GilroyRegular,
    marginLeft: 15,
  },
  formViewSectionLabelCustom: {
    flexDirection: "row",
    alignItems: "center",
  },
  formViewSectionLabelCustomText: {
    fontSize: 13,
    color: "#25a8ed",
    fontFamily: theme.fonts.GilroyLight,
  },
  buttonLogin: {
    width: "100%",
    height: 70,
    backgroundColor: "#25a8ed",
    borderRadius: 35,
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonLoginTitle: {
    color: "#fff",
    fontSize: 24,
    fontFamily: theme.fonts.GilroyBold,
  },
  viewOr: {
    width: "100%",
    marginVertical: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  viewOrText: {
    fontSize: 15,
    fontFamily: theme.fonts.GilroyLight,
    color: "rgba(0,0,0,0.77)",
  },
  socialsView: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  ButtonSocialView: {
    width: 55,
    height: 55,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    marginHorizontal: 10,
  },
  buttonGoogle: {
    backgroundColor: "rgb(248, 79, 55)",
  },
  buttonTwitter: {
    backgroundColor: "#1da1f2",
  },
  buttonFB: {
    backgroundColor: "#4267b2",
  },
  ButtonSocialViewText: {
    color: "#fff",
    fontFamily: theme.fonts.GilroyExtrabold,
    fontSize: 22,
  },
  container: {
    width: "100%",
    maxWidth: "100%",
  },
  inputControl: {
    width: "100%",
    height: 43,
    backgroundColor: "#F2F4F7",
    borderRadius: 20,
    paddingHorizontal: 14,
    color: "#000000",
    fontSize: 14,
    fontFamily: theme.fonts.GilroyRegular,
  },
  labelView: {
    paddingLeft: 14,
    marginBottom: 2,
    color: "#404040",
    fontSize: 14,
    lineHeight: 14,
    fontFamily: theme.fonts.GilroyRegular,
  },
  initialView: {
    marginTop: 15,
  },
  buttonSubmit: {
    width: "100%",
    height: 43,
    backgroundColor: "#24A8ED",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 41,
  },
  buttonSubmitText: {
    color: "#FFFFFF",
    fontSize: 14,
    fontFamily: theme.fonts.GilroyBold,
  },
});

export default styles;
