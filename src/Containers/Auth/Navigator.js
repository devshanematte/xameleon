import { createStackNavigator } from "react-navigation-stack";

import LoginScreen from "./LoginScreen";

export default createStackNavigator(
  {
    login: { screen: LoginScreen },
  },
  {
    initialRouteName: "login",
    headerMode: "none",
  }
);
