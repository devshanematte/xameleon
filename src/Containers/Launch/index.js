import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { View, Text, Image } from "react-native";
import { api } from "../../Services";
import styles from "./styles";
import { name as appName } from "../../../app.json";

const LaunchScreen = ({ navigation }) => {
  let dispatch = useDispatch();

  const authInit = async () => {
    const token = await api.getToken();
    api.updateAxiosConfig(token);

    if (token) {
      return navigation.navigate("user");
    }

    return navigation.navigate('auth');
  };

  useEffect(() => {
    setTimeout(() => {
      return authInit();
    }, 2000);

    return;
  }, []);

  return (
    <View style={styles.view}>
      <Image style={styles.logoApp} source={require("../../assets/logo.png")} />
      <Text style={styles.titleCopyright}>Хамелеон 2020 ©</Text>
    </View>
  );
};

export default LaunchScreen;
