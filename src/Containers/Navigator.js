// eslint-disable-next-line no-unused-vars
import React from "react";
import { createSwitchNavigator, createAppContainer } from "react-navigation";
import AuthNavigatorStack from "./Auth/Navigator";
import LaunchScreen from "./Launch";
import UserNavigatorStack from "./User/Navigator";

const AppNavigator = createAppContainer(
  createSwitchNavigator(
    {
      auth: { screen: AuthNavigatorStack },
      launch: { screen: LaunchScreen },
      user: { screen: UserNavigatorStack },
    },
    {
      initialRouteName: "launch",
    }
  )
);

export default AppNavigator;
