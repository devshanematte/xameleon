import React, {
	useMemo,
	useRef,
	useState,
	useEffect
} from 'react';
import { RNCamera } from 'react-native-camera';
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';

const CameraScreen = ({
	navigation
}) => {

	let refCamera = useRef();
	let [cameraHide, setCameraHide] = useState(true);
	let [barcodeValue, setBarcodeValue] = useState('');

	useEffect(()=>{
		renderCamera();
		return
	}, [])

	const renderCamera = () => {
		setTimeout(()=>{

			setCameraHide(false);

		}, 500);
	}

	const onBarCodeRead = (e) => {

		setBarcodeValue(e.data);
		return

	}

	return useMemo(()=>(
		<View style={styles.view}>

			<View style={styles.viewCamera}>
		        {
		        	!cameraHide ?
						<RNCamera
				          	ref={refCamera}
				          	style={styles.cameraStyle}
				          	onBarCodeRead={(e)=>{onBarCodeRead(e)}}
				        />	
				    :
				    	<View></View>
		        }
	        </View>

	        <View style={styles.barcodeView}>
	        	<Text style={styles.barcodeMainTitle}>Добавить необходимый товар</Text>
	        	<Text style={styles.barcodeDescription}> Отсканируйте штрихкод и сделайте фотографии товара</Text>
	        	<View style={styles.barcodeViewForm}>
	        		<Text style={styles.barcodeViewFormText}>{ barcodeValue != '' ? barcodeValue : 'Штрихкод' }</Text>
	        	</View>
	        </View>

	        <View style={styles.photosView}>
	        	
	        	<View style={styles.selectTypePhotoView}>
	        		
	        		<TouchableOpacity style={styles.viewcontent}>
	        			<Text style={styles.viewcontentTitle}>Штрихкод</Text>
	        			<View style={styles.viewcontentbutton}>
	        				<Icon style={styles.viewcontentbuttonIcon} name="camera" />
	        			</View>
	        		</TouchableOpacity>

	        		<TouchableOpacity style={styles.viewcontent}>
	        			<Text style={styles.viewcontentTitle}>Состав</Text>
	        			<View style={styles.viewcontentbutton}>
	        				<Icon style={styles.viewcontentbuttonIcon} name="camera" />
	        			</View>
	        		</TouchableOpacity>

	        		<TouchableOpacity style={styles.viewcontent}>
	        			<Text style={styles.viewcontentTitle}>Товар</Text>
	        			<View style={styles.viewcontentbutton}>
	        				<Icon style={styles.viewcontentbuttonIcon} name="camera" />
	        			</View>
	        		</TouchableOpacity>

	        	</View>

	        	<TouchableOpacity style={styles.buttonSaveBarCode}>
	        		<Text style={styles.buttonSaveBarCodeText}>Сохранить</Text>
	        	</TouchableOpacity>

	        	<TouchableOpacity onPress={()=>{navigation.goBack()}} style={styles.buttonCloseCamera}>
	        		<Text style={styles.buttonCloseCameraText}>Закрыть</Text>
	        	</TouchableOpacity>

	        </View>

		</View>
	), [renderCamera, cameraHide, barcodeValue]);
}

export default CameraScreen;