import React, {
	useMemo,
	useEffect,
	useState
} from 'react';
import {
	View,
	Text,
	TouchableOpacity,
	ScrollView,
	Image,
	VirtualizedList
} from 'react-native';
import styles from '../../styles';
import { useSelector, useDispatch } from 'react-redux';
import { api } from '../../../../../Services';

const AllComponent = ({
	navigation
}) => {

	const dispatch = useDispatch();
	let getCategories = useSelector((state)=>{
		return state.products
	});

	let newMapCategories = [];
	getCategories.categories.map((item, index)=>{
		if(item.sub){
			newMapCategories = [...newMapCategories, {
				...item,
				view:false,
				select:false,
				statusArrow:false
			}]
			return newMapCategories;
		}

		newMapCategories = [...newMapCategories, {
			...item,
			view:true,
			select:false,
			statusArrow:false
		}]

		return newMapCategories;
	})

	let [categories, setcategories] = useState(getCategories.categories);
	let [requestStatusCategories, setRequestStatusCategories] = useState(getCategories.requestStatusCategories);

	const selectCategory = (indexSelect) => {

		let newMapCategories = [];
		categories.map((item, index)=>{

			if(!categories[indexSelect].sub){

				if(categories[indexSelect]._id == item.cat_id){
					newMapCategories = [...newMapCategories, {
						...item,
						select:!categories[indexSelect].select
					}]
					return newMapCategories
				}

			}

			if(index == indexSelect){
				newMapCategories = [...newMapCategories, {
					...item,
					select:!item.select
				}]
				return newMapCategories
			}

			newMapCategories = [...newMapCategories, item];
			return newMapCategories

		});

		setcategories(newMapCategories);
		return

	}

	const viewSubCat = (indexCat, statusArrow, mainCatId) => {

		let newMapCategories = [];

		categories.map((item, index)=>{

			if(String(mainCatId) == String(item.cat_id)){

				newMapCategories = [...newMapCategories, {
					...item,
					view:!item.view
				}];

				return newMapCategories;

			}

			newMapCategories = [...newMapCategories, item];
			return newMapCategories;

		})
		newMapCategories[indexCat].statusArrow = !newMapCategories[indexCat].statusArrow;
		setcategories(newMapCategories);
		return

	}

	const resetFilter = () => {

		let newMapCategories = [];

		categories.map((item, index)=>{

			newMapCategories = [...newMapCategories, {
				...item,
				select:false
			}]

			return newMapCategories

		})

		setcategories(newMapCategories);
		return

	}

	return (

		<View style={styles.categoriesStyleView}>
			<ScrollView showsVerticalScrollIndicator={false}>

				<View style={styles.drawerTopHeaderFilter}>
					<Image style={styles.drawerTopHeaderFilterImage} source={require('../../../../../assets/filter.png')} />
					<Text style={styles.drawerTopHeaderFilterText}>Фильтр по выбранному</Text>
					<TouchableOpacity onPress={()=>{resetFilter()}}>
						<Text style={styles.drawerTopHeaderFilterTextResetButton}>(сбросить)</Text>
					</TouchableOpacity>
				</View>

				{
					categories.map((item, index)=>{

			        	return(
							<View key={index}>
								{
									item.sub && item.view ?
										<TouchableOpacity onPress={()=>{selectCategory(index)}} style={[styles.categoryButton, {width:'98.9%'}]}>
											<Text style={[styles.subCategoryButtonText, item.select ? styles.ButtonTextSelected : {}]}>{item.name} (0)</Text>
											<View style={styles.iconViewCircle}>
											{
												item.select ?
													<View style={styles.iconViewCircleCenter}></View>
												:
													<View></View>
											}
											</View>
										</TouchableOpacity>
									: item.view ?
										<View style={[styles.viewMainCategory, {marginTop:20}]}>
											<TouchableOpacity onPress={()=>{viewSubCat(index, item.statusArrow, item._id)}} style={styles.viewMainCategoryIcon}>
												<Image style={styles.viewMainCategoryIconStyle} source={item.statusArrow ? require('../../../../../assets/icon-up.png'): require('../../../../../assets/icon-down.png')} />
											</TouchableOpacity>
											<TouchableOpacity  onPress={()=>{selectCategory(index)}} style={styles.categoryButton}>
												<Text style={[styles.categoryButtonText, item.select ? styles.ButtonTextSelected : {}]}>{ item.name } (0)</Text>
												<View style={styles.iconViewCircle}>
												{
													item.select ?
														<View style={styles.iconViewCircleCenter}></View>
													:
														<View></View>
												}
												</View>
											</TouchableOpacity>
										</View>
									:	<View></View>
								}
							</View>
			        	)

					})
				}

				<View style={styles.footerDrawer}>
					<TouchableOpacity style={[styles.button, styles.buttonBlue]}>
						<Text style={[styles.buttonText]}>Добавить товар из каталога</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[styles.button]}>
						<Text style={[styles.buttonText, styles.buttonTextDefault]}>Добавить необходимый товар</Text>
					</TouchableOpacity>

					<View>
						<Text style={styles.textDescription}>Если отсутствует товар в общем каталоге ты можешь его добавить с помощью кнопки "Добавить необходимый товар" и запросить доставку магазин</Text>
					</View>
				</View>

			</ScrollView>

	    </View>

	)

};

export default AllComponent;