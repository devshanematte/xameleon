import React, {
	useMemo,
	useEffect,
	useState
} from 'react';
import {
	View,
	Text,
	TouchableOpacity,
	ScrollView,
	VirtualizedList,
	Image
} from 'react-native';
import styles from '../../styles';
import { useSelector, useDispatch } from 'react-redux';
import { api } from '../../../../../Services';

let defaultCategories = [{
	name:'Необходимые товары',
	_id:1,
	sub:false,
	view:true,
	select:false,
	statusArrow:false
},{
	name:'Необходимые товары',
	cat_id:1,
	sub:true,
	view:false,
	select:false,
	statusArrow:false
},{
	name:'Запрошена поставка',
	cat_id:1,
	sub:true,
	view:false,
	select:false,
	statusArrow:false
},{
	name:'Неотправленные',
	cat_id:1,
	sub:true,
	view:false,
	select:false,
	statusArrow:false
},{
	name:'Отмененные',
	cat_id:1,
	sub:true,
	view:false,
	select:false,
	statusArrow:false
}];

const RationComponent = ({
	navigation
}) => {

	const dispatch = useDispatch();
	let getCategories = useSelector((state)=>{
		return state.products
	});

	let newMapCategories = [];
	getCategories.categories.map((item, index)=>{
		if(item.sub){
			newMapCategories = [...newMapCategories, {
				...item,
				view:false,
				select:false,
				statusArrow:false
			}]
			return newMapCategories;
		}

		newMapCategories = [...newMapCategories, {
			...item,
			view:true,
			select:false,
			statusArrow:false
		}]

		return newMapCategories;
	})

	let [categories, setcategories] = useState(getCategories.categories);
	let [requestStatusCategories, setRequestStatusCategories] = useState(getCategories.requestStatusCategories);
	let [defaultCategoriesList, setDefaultCategoriesList] = useState(defaultCategories);

	const selectCategory = (indexSelect) => {

		let newMapCategories = [];
		categories.map((item, index)=>{

			if(!categories[indexSelect].sub){

				if(categories[indexSelect]._id == item.cat_id){
					newMapCategories = [...newMapCategories, {
						...item,
						select:!categories[indexSelect].select
					}]
					return newMapCategories
				}

			}

			if(index == indexSelect){
				newMapCategories = [...newMapCategories, {
					...item,
					select:!item.select
				}]
				return newMapCategories
			}

			newMapCategories = [...newMapCategories, item];
			return newMapCategories

		});

		setcategories(newMapCategories);
		return

	}

	const selectCategoryDefault = (indexSelect) => {
		let newMapCategories = [];
		defaultCategoriesList.map((item, index)=>{

			if(!defaultCategoriesList[indexSelect].sub){

				if(defaultCategoriesList[indexSelect]._id == item.cat_id){
					newMapCategories = [...newMapCategories, {
						...item,
						select:!defaultCategoriesList[indexSelect].select
					}]
					return newMapCategories
				}

			}

			if(index == indexSelect){
				newMapCategories = [...newMapCategories, {
					...item,
					select:!item.select
				}]
				return newMapCategories
			}

			newMapCategories = [...newMapCategories, item];
			return newMapCategories

		});

		setDefaultCategoriesList(newMapCategories);
		return
	}

	const viewSubCat = (indexCat, statusArrow, mainCatId) => {

		let newMapCategories = [];

		defaultCategoriesList.map((item, index)=>{

			if(String(mainCatId) == String(item.cat_id)){

				newMapCategories = [...newMapCategories, {
					...item,
					view:!item.view
				}];

				return newMapCategories;

			}

			newMapCategories = [...newMapCategories, item];
			return newMapCategories;

		})
		newMapCategories[indexCat].statusArrow = !newMapCategories[indexCat].statusArrow;
		setDefaultCategoriesList(newMapCategories);
		return

	}

	return (

		<View style={styles.categoriesStyleView}>
			<ScrollView showsVerticalScrollIndicator={false}>

				{
					categories.map((item, index)=>{

			        	return(
							<View key={index}>
								<View style={[styles.viewMainCategory, styles.viewMainCategoryRation]}>
									{
										!item.sub ?
											<TouchableOpacity  onPress={()=>{selectCategory(index)}} style={[styles.categoryButton, styles.categoryButtonRation]}>
												<Text style={[styles.categoryButtonText, styles.categoryButtonTextRation, item.select ? styles.ButtonTextSelected : {}]}>{ item.name } (0)</Text>
												<View style={styles.iconViewCircle}>
												{
													item.select ?
														<View style={styles.iconViewCircleCenter}></View>
													:
														<View></View>
												}
												</View>
											</TouchableOpacity>
										:
											<View></View>
									}
								</View>
							</View>
			        	)

					})
				}

				{
					defaultCategoriesList.map((item, index)=>{
						return (
							<View key={index}>
								{
									item.sub && item.view ?
										<TouchableOpacity onPress={()=>{selectCategoryDefault(index)}} style={[styles.categoryButton, {width:'98.9%'}]}>
											<Text style={[styles.subCategoryButtonText, item.select ? styles.ButtonTextSelected : {}]}>{item.name} (0)</Text>
											<View style={styles.iconViewCircle}>
											{
												item.select ?
													<View style={styles.iconViewCircleCenter}></View>
												:
													<View></View>
											}
											</View>
										</TouchableOpacity>
									: item.view ?
										<View style={[styles.viewMainCategory, {marginTop:20}]}>
											<TouchableOpacity onPress={()=>{viewSubCat(index, item.statusArrow, item._id)}} style={styles.viewMainCategoryIcon}>
												<Image style={styles.viewMainCategoryIconStyle} source={item.statusArrow ? require('../../../../../assets/icon-up.png'): require('../../../../../assets/icon-down.png')} />
											</TouchableOpacity>
											<TouchableOpacity  style={styles.categoryButton}>
												<Text style={[styles.categoryButtonText, item.select ? styles.ButtonTextSelected : {}]}>{ item.name } (0)</Text>
												{
													/*
												<View style={styles.iconViewCircle}>
												{

													item.select ?
														<View style={styles.iconViewCircleCenter}></View>
													:
														<View></View>
												}
												</View>
													*/
												}
											</TouchableOpacity>
										</View>
									:	<View></View>
								}
							</View>
						)
					})
				}

				<View style={styles.footerDrawer}>
					<TouchableOpacity style={[styles.button, styles.buttonBlue]}>
						<Text style={[styles.buttonText]}>Добавить товар из каталога</Text>
					</TouchableOpacity>
				</View>

			</ScrollView>

	    </View>

	)

};

export default RationComponent;