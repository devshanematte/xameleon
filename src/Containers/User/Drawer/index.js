import React, {
	useMemo,
	useEffect
} from 'react';
import {
	View,
	Text,
	TouchableOpacity,
	ScrollView,
	VirtualizedList
} from 'react-native';
import styles from './styles';
import { useSelector, useDispatch } from 'react-redux';
import { api } from '../../../Services';
import RationComponent from './helpers/Ration';
import AllComponent from './helpers/All';

const Drawer = ({
	navigation
}) => {

	const dispatch = useDispatch();
	let getCategories = useSelector((state)=>{
		return state.products
	});
	let getRoutes = useSelector((state)=>{
		return state.routes
	});

	let {
		categories,
		requestStatusCategories,
	} = getCategories;

	useEffect(()=>{
		dispatch(api.products.getCategories());
		return
	}, []);

	return useMemo(()=>(
		<View style={styles.view}>
			<View style={styles.viewCategories}>
				{
					requestStatusCategories ?
						<View style={styles.loading}>
							<Text style={styles.loadingText}>Подождите, загружаем...</Text>
						</View>
					: categories && categories.length && getRoutes.currentNavigation == 'ration' ?
						<RationComponent/>
					: categories && categories.length ?
						<AllComponent/>
					:
						<View style={styles.noCategoriesView}>
							<Text style={styles.noCategoriesText}>Категорий нет</Text>
						</View>
				}
			</View>
		</View>
	), [categories, requestStatusCategories, getRoutes])

};

export default Drawer;