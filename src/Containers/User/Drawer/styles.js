import { 
	StyleSheet, 
	Dimensions 
} from 'react-native';
import { theme } from '../../../Services';

let { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
	view:{
		flex:1,
		width:'80%',
		backgroundColor:'#fff',
		justifyContent:'space-between',
		marginTop:90,
		position:'relative',

		shadowColor: "#000",
		shadowOffset: {
			width: 25,
			height: 15,
		},
		shadowOpacity: 1,
		shadowRadius: 21.14,

		elevation: 14,
	},
	bar:{
		position:'absolute',
		top:10,
		right:-30,
		width:30,
		height:30,
		backgroundColor:'red',
		zIndex:1000
	},
	viewCategories:{
		position:'absolute',
		top:0,
		left:0,
		width:'100%',
		height:'100%'
	},
	viewFooterDrawer:{
		position:'absolute',
		bottom:0,
		left:0,
		width:'100%',
		height:'35%',
		padding:25
	},
	button:{
		width:'100%',
		height:57,
		backgroundColor:'rgba(0,0,0,0.1)',
		borderRadius:24,
		justifyContent:'center',
		alignItems:'center',
		marginBottom:15
	},
	buttonBlue:{
		backgroundColor:'#23a8ee'
	},
	buttonText:{
		fontSize:16,
		color:'#fff',
		fontFamily:theme.fonts.GilroyMedium
	},
	buttonTextDefault:{
		color:'rgba(0,0,0,0.47)'
	},
	textDescription:{
		fontSize:16,
		color:'rgba(0,0,0,0.36)',
		fontFamily:theme.fonts.GilroyMedium
	},
	loading:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
	},
	loadingText:{
		fontSize:16,
		color:'rgba(0,0,0,0.36)',
		fontFamily:theme.fonts.GilroyMedium
	},
	noCategoriesView:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
	},
	noCategoriesText:{
		fontSize:16,
		color:'rgba(0,0,0,0.36)',
		fontFamily:theme.fonts.GilroyMedium
	},
	categoriesStyleView:{
		flex:1,
		padding:15
	},
	categoryButton:{
		width:'100%',
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'space-between',
		marginBottom:5
	},
	categoryButtonText:{
		width:'65%',
		color:'#404040',
		fontSize:19,
		fontFamily:theme.fonts.GilroyMedium
	},
	categoryButtonTextRation:{
		width:'88%'
	},
	subCategoryButtonText:{
		width:'80%',
		color:'#404040',
		fontSize:17,
		fontFamily:theme.fonts.GilroyRegular,
		marginLeft:30
	},
	iconViewCircle:{
		width:26,
		height:26,
		backgroundColor:'rgba(0,0,0,0.05)',
		borderRadius:13,
		justifyContent:'center',
		alignItems:'center'
	},
	iconViewCircleCenter:{
		width:15,
		height:15,
		borderRadius:7,
		backgroundColor:'#23a8ee'
	},
	footerDrawer:{
		marginTop:25
	},
	viewMainCategory:{
		width:'90%',
		flexDirection:'row',
		alignItems:'center'
	},
	viewMainCategoryRation:{
		width:'100%'
	},
	viewMainCategoryIcon:{
		width:25,
		height:25,
		marginRight:5
	},
	viewMainCategoryIconStyle:{
		width:'100%',
		height:'100%',
		resizeMode:'contain'
	},
	ButtonTextSelected:{
		color:'#24A8ED'
	},
	categoryButtonRation:{
		marginBottom:22,
		alignItems:'center',
		width:'100%',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	drawerTopHeaderFilter:{
		width:'100%',
		flexDirection:'row',
		alignItems:'center'
	},
	drawerTopHeaderFilterImage:{
		width:30,
		height:41,
		resizeMode:'contain',
		marginRight:10
	},
	drawerTopHeaderFilterText:{
		color:'#404040',
		fontSize:17,
		fontFamily:theme.fonts.GilroyBold,
		marginRight:5
	},
	drawerTopHeaderFilterTextResetButton:{
		color:'#DC4418',
		fontSize:17,
		fontFamily:theme.fonts.GilroyRegular
	}
});

export default styles;
