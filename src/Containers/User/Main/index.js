import React, { useMemo } from "react";
import { useDispatch } from "react-redux";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Linking,
} from "react-native";
import styles from "./styles";
import { api } from "../../../Services";
import {
  DefaultHeader,
  TimeAndDrawerEvent,
  CopyFooter,
} from "../../../Components";
import Icon from "react-native-vector-icons/FontAwesome5";
import { default as HomeImage } from "../../../assets/home-image.svg";

const Main = ({ navigation }) => {
  let dispatch = useDispatch();

  return useMemo(
    () => (
      <View style={styles.view}>
        <DefaultHeader navigation={navigation} />

        <View style={styles.viewContent}>
          <ScrollView>
            <View style={styles.viewContentPadding}>
              <Text style={styles.textHome}>
                Всем привет, меня зовут ХАМЕЛЕОН! Я знакомлю продуктовые
                магазины с рационами людей для урощения их взаимодействия.
              </Text>

              <Text style={styles.textHome}>
                Первым шагом нам необходимо собрать твой рацион из продуктов
                питания и хозяйственных товаров. Для этого в{" "}
                <Text
                  style={styles.textHomeLink}
                  onPress={() => Linking.openURL("http://google.com")}
                >
                  ТВОЕМ РАЦИОНЕ{" "}
                </Text>
                отсканируй штрихкод необходимого товара и он добавится к тебе в
                рацион. Если товар отсутствует в{" "}
                <Text style={styles.textHomeLinkB}>ОБЩЕМ КАТАЛОГЕ</Text> -
                просто отсканируй его штрихкод, сделай необходимые фотографии и
                отправь его нам, мы добавим его в общий каталог и он
                автоматически появится в твоем рационе. Товар можно также
                добавлять в рацион из общего каталога с помощью поиска.
              </Text>
              <Text style={styles.textHome}>
                Скоро выйдет моя расширенная версия и мы сможем делать покупки
                необходимых товаров с доставкой на дом.
              </Text>

              <HomeImage width="100%" style={styles.homeImageStyle} />

              <CopyFooter />
            </View>
          </ScrollView>
        </View>
      </View>
    ),
    []
  );
};

export default Main;
