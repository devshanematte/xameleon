import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

import Drawer from './Drawer';
import Main from './Main';
import CameraScreen from './Camera';
import Products from './Products';
import Ration from './Ration';

export default createStackNavigator({
  drawer: createDrawerNavigator({
    home: {
      screen: Main
    },
    products:{
      screen:Products
    },
    ration:{
      screen:Ration
    }
  }, {
    contentComponent: Drawer,
    overlayColor: 'rgba(0,0,0,0.01)',
    drawerWidth: '100%',
    drawerBackgroundColor: {
        light: 'transparent',
        dark: 'transparent',
    },
  }),
  camera:{
    screen:CameraScreen
  }
}, {
    navigationOptions: {
      header: null,
    },
    cardStyle: {
      backgroundColor: '#fff'
    },
    headerMode: 'none',
  },
);
