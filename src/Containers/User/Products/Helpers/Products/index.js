import React, { useMemo } from "react";
import { View, Text } from "react-native";
import styles from "./styles";
import { useSelector } from "react-redux";

const ProductsHelper = () => {
  const getProducts = useSelector((state) => {
    return state.products.list;
  });

  return useMemo(() => (
    <View style={styles.view}>
      {getProducts && getProducts.length ? (
        <View>
          <Text>Products</Text>
        </View>
      ) : (
        <View style={styles.noProductsView}>
          <Text style={styles.noProductsViewText}>Продуктов нет</Text>
        </View>
      )}
    </View>
  ));
};

export default ProductsHelper;
