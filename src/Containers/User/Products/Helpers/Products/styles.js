import { StyleSheet, Dimensions } from "react-native";
import { theme } from "../../../../../Services";

let { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  view: {
    flex: 1,
    width: "100%",
    minHeight: height - 230,
  },
  noProductsView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  noProductsViewText: {
    fontSize: 18,
    color: "rgba(0,0,0,0.7)",
    fontFamily: theme.fonts.GilroyRegular,
  },
});

export default styles;
