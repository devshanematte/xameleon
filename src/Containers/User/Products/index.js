import React, {
	useMemo
} from 'react';
import { useDispatch } from 'react-redux';
import {
	View,
	Text,
	ScrollView,
	TouchableOpacity
} from 'react-native';
import styles from './styles';
import { api } from '../../../Services';
import { 
	DefaultHeader,
	TimeAndDrawerEvent,
	CopyFooter
} from '../../../Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { default as HomeImage } from '../../../assets/home-image.svg';

import ProductsHelper from './Helpers/Products';

const Products = ({
	navigation,
}) => {

	let dispatch = useDispatch();

	return useMemo(()=>(
		<View style={styles.view}>
			<DefaultHeader navigation={navigation} />

			<View style={styles.viewContent}>
				<ScrollView>
					<TimeAndDrawerEvent allAndSearch navigation={navigation}/>
					<ProductsHelper/>
					<CopyFooter/>
				</ScrollView>

			</View>

		</View>
	), [])
}

export default Products;