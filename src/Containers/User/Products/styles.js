import { 
	StyleSheet, 
	Dimensions 
} from 'react-native';
import { theme } from '../../../Services';

let { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
	view:{
		flex:1,
		width:'100%',
		backgroundColor:'#f2f2f2'
	},
	viewContent:{
		flex:1,
		width:'100%'
	},
	buttonBarCode:{
		position:'absolute',
		zIndex:100,
		bottom:20,
		right:20,
		width:80,
		height:80,
		borderRadius:40,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#23a8ee',

		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.15,
		shadowRadius: 3.84,

		elevation: 8,
	},
	buttonBarCodeIcon:{
		fontSize:35,
		color:'#fff'
	},
	textHome:{
		color:'#404040',
		fontSize:24,
		fontFamily:theme.fonts.GilroyRegular
	},
	viewContentPadding:{
		padding:20
	},
	homeImageStyle:{
		width:'100%',
		marginTop:-100,
		marginBottom:-100
	}
});

export default styles;
