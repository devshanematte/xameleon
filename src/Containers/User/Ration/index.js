import React, {
	useMemo
} from 'react';
import { useDispatch } from 'react-redux';
import {
	View,
	Text,
	ScrollView,
	Image,
	TouchableOpacity
} from 'react-native';
import styles from './styles';
import { api } from '../../../Services';
import { 
	DefaultHeader,
	TimeAndDrawerEvent,
	CopyFooter
} from '../../../Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { default as HomeImage } from '../../../assets/home-image.svg';

import ProductsHelper from './Helpers/Products';

const Ration = ({
	navigation,
}) => {

	let dispatch = useDispatch();

	return useMemo(()=>(
		<View style={styles.view}>
			<DefaultHeader navigation={navigation} />

			<View style={styles.viewContent}>
				<ScrollView>
					<TimeAndDrawerEvent navigation={navigation}/>
					<ProductsHelper/>
					<CopyFooter/>
				</ScrollView>

				<TouchableOpacity onPress={()=>{navigation.navigate('camera')}} style={styles.buttonBarCode}>
					<Image source={require('../../../assets/barcode.png')} style={styles.buttonBarCodeImage} />
				</TouchableOpacity>

			</View>

		</View>
	), [])
}

export default Ration;