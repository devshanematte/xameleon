import axios from '../axios';
import { NavigationService, api } from '../../';

export default {
	login: (params, type) => async dispatch => {

		if(type == 'authentication'){

			let postParams = {
				email:params.email,
				password:params.password
			}

			dispatch({
				type:'REQUEST_AUTH',
				status:true
			});

			try{

				let { data } = await axios.post('/auth/authentication', postParams);

				await api.setToken(data.token);
				api.updateAxiosConfig(data.token);

				dispatch({
					type:'UPDATE_USER',
					user:data.user
				});

				dispatch({
					type:'LOGGED_IN',
					logged:true
				});

				dispatch({
					type:'REQUEST_AUTH',
					status:false
				});

				return NavigationService.navigate('launch');

			}catch(err){
				console.log(222, err)
				alert('Данные введены неверно.');
				return dispatch({
					type:'REQUEST_AUTH',
					status:false
				});
			}

			return

		}

		let postParams = {
			email: params.email,
			password: params.password,
			lastname: params.lastname,
			firstname: params.firstname
		}

		try{

			let { data } = await axios.post('/auth/signup', postParams);

			console.log(2233, data)
			alert('Регистрация прошла успешно.');
			return

			await api.setToken(data.token);
			api.updateAxiosConfig(data.token);

			dispatch({
				type:'UPDATE_USER',
				user:data.user
			});

			dispatch({
				type:'LOGGED_IN',
				logged:true
			});

			dispatch({
				type:'REQUEST_AUTH',
				status:false
			});

			return NavigationService.navigate('launch');

		}catch(err){
			console.log(222, err)
			return dispatch({
				type:'REQUEST_AUTH',
				status:false
			});
		}

	},	
	logout: () => async dispatch => {

		await api.removeToken();

		dispatch({
			type:'UPDATE_USER',
			user:null
		});

		dispatch({
			type:'LOGGED_IN',
			logged:false
		});

		return NavigationService.navigate('launch');

	}
}