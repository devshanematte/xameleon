import { AsyncStorage } from 'react-native';
import { updateAxiosConfig } from './axios';
import auth from './auth';
import products from './products';

export default {
	getToken: async () => {
		const token = await AsyncStorage.getItem('Token')
		return token
	},
	setToken: async (token) => {
		const setToken = await AsyncStorage.setItem('Token', token)
		return setToken
	},
	removeToken: async () => {
		const removeToken = await AsyncStorage.removeItem('Token')
		return removeToken
	},
	updateAxiosConfig,
	auth,
	products
}