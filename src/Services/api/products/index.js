import axios from "../axios";
import { NavigationService, api } from "../../";

export default {
  getCategories: () => async (dispatch) => {
    dispatch({
      type: "REQUEST_STATUS_CATEGORIES",
      status: true,
    });

    try {
      let { data } = await axios.get("/categories");

      let reMapCats = [];
      console.log(data);
      data.map((cat) => {
        reMapCats = [
          ...reMapCats,
          {
            ...cat,
            sub: false,
            select: false,
            statusArrow: false,
            view: true,
          },
        ];

        if (cat.sub_categories && cat.sub_categories.length) {
          cat.sub_categories.map((sub_cat) => {
            reMapCats = [
              ...reMapCats,
              {
                ...sub_cat,
                sub: true,
                select: false,
                view: false,
                cat_id: cat._id,
              },
            ];
          });
        }
      });

      dispatch({
        type: "UPDATE_CATEGORIES",
        data: reMapCats,
      });

      return dispatch({
        type: "REQUEST_STATUS_CATEGORIES",
        status: false,
      });
    } catch (err) {
      dispatch({
        type: "REQUEST_STATUS_CATEGORIES",
        status: false,
      });
    }
  },
  selectCategory: (index) => async (dispatch) => {
    return dispatch({
      type: "SELECT_CATEGORY",
      index,
    });
  },
};
