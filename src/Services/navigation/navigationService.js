import { NavigationActions, SwitchActions } from 'react-navigation';

// eslint-disable-next-line no-underscore-dangle
let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params = {}) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
}

function goBack(arg) {
  _navigator.dispatch(NavigationActions.back(arg));
}

function reset(routeName) {
  _navigator.dispatch(SwitchActions.jumpTo({ routeName }));
}

const NavigationService = {
  navigate,
  goBack,
  reset,
  setTopLevelNavigator,
};

export default NavigationService;
