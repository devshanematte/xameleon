const initialState = {
	logged:false,

	corectEmail:true,

	requestAuth:false,
	errorAuth:false,
	recoveryError:false,

}

const auth = (state = initialState, action) => {
	switch(action.type){
		case 'CORRECT_EMAIL_AUTH_FORM' :
			return {
				...state,
				corectEmail:action.status
			}
		case 'REQUEST_AUTH' :
			return {
				...state,
				requestAuth:action.status
			}
		case 'ERROR_AUTH_USER' :
			return {
				...state,
				errorAuth:action.status
			}
		case 'ERROR_RECOVERY_USER' :
			return {
				...state,
				recoveryError:action.status
			}
		case 'LOGGED_IN' :
			return {
				...state,
				logged:action.logged
			}

		default :
			return state
	}
}

export default auth;