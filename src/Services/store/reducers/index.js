import { combineReducers } from 'redux';
import app from './app';
import auth from './auth';
import user from './user';
import products from './products';
import routes from './routes';

export default combineReducers({
	app,
	auth,
	user,
	products,
	routes
})
