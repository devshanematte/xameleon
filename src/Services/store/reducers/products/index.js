const initialState = {
	list:null,
	categories:null,

	requestStatusProducts:false,
	requestStatusCategories:false
}

const products = (state = initialState, action) => {
	switch(action.type){
		case 'REQUEST_STATUS_CATEGORIES' :
			return {
				...state,
				requestStatusCategories:action.status
			}
		case 'REQUEST_STATUS_PRODUCTS' :
			return {
				...state,
				requestStatusProducts:action.status
			}
		case 'UPDATE_CATEGORIES' :
			return {
				...state,
				categories:action.data
			}
		case 'SELECT_CATEGORY' :

			return {
				...state,
				categories:state.categories.map((item, index)=>{

					if(index == action.index){
						return {
							...item,
							select:!item.select
						}
					}

					return {
						...item
					}
				})
			}
		default :
			return state
	}
}

export default products;