const initialState = {
	currentNavigation:'home'
}

const routes = (state = initialState, action) => {
	switch(action.type){

		case 'UPDATE_CURRENT_NAVIGATION' :
			return {
				...state,
				currentNavigation:action.route
			}

		default :
			return state
	}
}

export default routes;