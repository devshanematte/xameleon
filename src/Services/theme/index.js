const fonts = {
	"GilroyBlack": 				"Gilroy-Black",
	"GilroyBlackItalic": 		"Gilroy-BlackItalic",
	"GilroyBold": 				"Gilroy-Bold",
	"GilroyBoldItalic": 		"Gilroy-BoldItalic",
	"GilroyExtrabold": 			"Gilroy-Extrabold",
	"GilroyExtraboldItalic": 	"Gilroy-ExtraboldItalic",
	"GilroyHeavy": 				"Gilroy-Heavy",
	"GilroyHeavyItalic": 		"Gilroy-HeavyItalic",
	"GilroyLight": 				"Gilroy-Light",
	"GilroyLightItalic": 		"Gilroy-LightItalic",
	"GilroyMedium": 			"Gilroy-Medium",
	"GilroyMediumItalic": 		"Gilroy-MediumItalic",
	"GilroyRegular": 			"Gilroy-Regular",
	"GilroyRegularItalic": 		"Gilroy-RegularItalic",
	"GilroySemibold": 			"Gilroy-Semibold",
	"GilroySemiboldItalic": 	"Gilroy-SemiboldItalic",
	"GilroyThin": 				"Gilroy-Thin",
	"GilroyThinItalic": 		"Gilroy-ThinItalic",
	"Gilroy-UltraLightItalic": 	"Gilroy-UltraLightItalic"
}

const colors = {
	
}

export default {
	colors,
	fonts
}